# nomad-exec

A repo to provide a consistent interface to scripts for nomad jobs. These scripts
can be run with the `exec` and `raw_exec` drivers. This repo can be used in an
`artifact` stanza as a means of providing the actual code to the builds.


### Essentials

* a script that builds a chroot with mkosi and uploads it to Spaces (raw_exec)
* a script that fetches that chroot from Spaces and uses systemd-nspawn to 
  run our custom chemist-runner program inside the container

### What About Docker?

Docker containers can run rpm and debian builds using the `docker` driver, so
we don't need to reference those build types here.

