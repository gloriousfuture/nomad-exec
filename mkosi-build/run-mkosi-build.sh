#!/bin/bash

echo "vars:"
echo "NOMAD_ALLOC_DIR: $NOMAD_ALLOC_DIR"
echo "NOMAD_TASK_DIR: $NOMAD_TASK_DIR"
echo "STARTING MKOSI BUILD"

cd local/repo/mkosi-build

pip install git+https://github.com/systemd/mkosi.git@v4

echo "WHICH MKOSI"
which mkosi

mkosi

# upload image to spaces when done


